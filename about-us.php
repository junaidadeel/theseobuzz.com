<!doctype html>
<html lang="en">
   <head>
      <title>The Seo Buzz</title>
      <meta name="description" content="">

    <?php
    include __DIR__ . '/include/header.php'
    ?>

    <section class="main_banner" style="background-image: url(images/webp/inner_banner/aboutus.webp);  height: 421px;">
        <div class="container">
            <div class="row align-items-center justify-content-center justify-content-xl-between justify-content-lg-between">
                <div class="col-lg-6">
                    <div class="bnr_content" data-aos="fade-right" data-aos-duration="1500">
                        <h3><span class="text_1">About Us</span></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="about_service">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="marketing_services_title sec_title" data-aos="fade-right" data-aos-duration="1500">
                        <h3>Grow Your Business Through <br><span>Our Experienced SEO Experts</span></h3>
                        <p>At SEO Buzz, we're a team of skilled digital marketing experts with great flair for search engine optimization strategy permitting businesses all over the globe to make informed digital marketing solutions. <br><br><br><br>The SEO Buzz prioritizes building and maintaining customer relationships over anything else. We tend to fully cater to our clients by being a one-stop-all digital marketing, SEO, and Social Media Marketing agency, all at the same place. To put it simply, we are engrossed in getting our clients the best value for their money, that too at the lowest possible costs. This, in turn, helps us become the best SEO agency and present ourselves as a global phenomenon.</p>
                    </div>
                </div>
                <div class="col-md-5 about_serv_col d-none d-xl-block d-lg-block">
                    <div class="about_serv_img">
                        <img src="images/webp/about_serv_img/aboutus.webp" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/include/projects.php'?>


    <?php include __DIR__ . '/include/our_team.php'?>


    <div class="video_presentation about-video">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="video_box" data-aos="fade-up" data-aos-duration="1500">
                        <h3>Watch This Video Presentation <br>To Know More</h3>
                        <a href="javascript:;"><i class="fas fa-play"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php include __DIR__ . '/include/cta.php' ?>


    <?php include __DIR__ . '/include/testimonial.php' ?>


    <?php include __DIR__ . '/include/clients.php' ?>


        <?php include __DIR__ . '/include/footer.php' ?>
