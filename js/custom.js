
$(".testi_slider").owlCarousel({
        loop: true,
        margin: 1,
        nav: true,
        navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
        dots: false,
        autoplay:false,
        autoplayTimeout:4500,
        autoplayHoverPause:false,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 1
            },
            992: {
                items: 2
            }
        }
    });

$(".service_slider").owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
        dots: false,
        autoplay:false,
        autoplayTimeout:4500,
        autoplayHoverPause:false,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 1
            },
            992: {
                items: 2
            },
            1199: {
                items: 3
            }
        }
    });

$(window).width() <= 991 && 0 != ".responsive_slider_service".length && ($(".responsive_slider_service").addClass("owl-carousel owl-theme"), $(".responsive_slider_service").owlCarousel({
    loop: !1,
    margin: 0,
    nav: !1,
    dots: !0,
    mouseDrag: !0,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 1
        },
        768: {
            items: 2
        },
        992: {
            items: 3
        }
    }
}));

$(window).width() <= 991 && 0 != ".clients_slider".length && ($(".clients_slider").addClass("owl-carousel owl-theme"), $(".clients_slider").owlCarousel({
    loop: !1,
    margin: 0,
    nav: !1,
    dots: !0,
    mouseDrag: !0,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 1
        },
        768: {
            items: 2
        },
        992: {
            items: 3
        }
    }
}));

$(window).width() <= 991 && 0 != ".responsive_slider_projects".length && ($(".responsive_slider_projects").addClass("owl-carousel owl-theme"), $(".responsive_slider_projects").owlCarousel({
    loop: !1,
    margin: 0,
    nav: !1,
    dots: !0,
    mouseDrag: !0,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 2
        },
        768: {
            items: 3
        },
        992: {
            items: 3
        }
    }
}));
$(window).width() <= 991 && 0 != ".responsive_slider_teams".length && ($(".responsive_slider_teams").addClass("owl-carousel owl-theme"), $(".responsive_slider_teams").owlCarousel({
    loop: !1,
    margin: 0,
    nav: !1,
    dots: !0,
    mouseDrag: !0,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 1
        },
        768: {
            items: 2
        },
        992: {
            items: 3
        }
    }
}));

$(window).width() <= 991 && 0 != ".responsive_slider_pkg".length && ($(".responsive_slider_pkg").addClass("owl-carousel owl-theme"), $(".responsive_slider_pkg").owlCarousel({
    loop: !1,
    margin: 0,
    nav: !1,
    dots: !0,
    mouseDrag: !0,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 1
        },
        768: {
            items: 2
        },
        992: {
            items: 3
        }
    }
}));
$(window).width() <= 991 && 0 != ".responsive_slider_blog".length && ($(".responsive_slider_blog").addClass("owl-carousel owl-theme"), $(".responsive_slider_blog").owlCarousel({
    loop: !1,
    margin: 0,
    nav: !1,
    dots: !0,
    mouseDrag: !0,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 1
        },
        768: {
            items: 2
        },
        992: {
            items: 3
        }
    }
}));

$(window).width() <= 575 && 0 != ".responsive_slider_marketing".length && ($(".responsive_slider_marketing").addClass("owl-carousel owl-theme"), $(".responsive_slider_marketing").owlCarousel({
    loop: !1,
    margin: 0,
    nav: !1,
    dots: !0,
    mouseDrag: !0,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 1
        },
        768: {
            items: 1
        },
        992: {
            items: 3
        }
    }
}));

AOS.init({
    disable: function() {
        return window.innerWidth < 991;
    }
});

$(document).ready(function() {
  $("#messageButton").click(function() {
    $("#messagefield").fadeToggle();
  });
});


$(".package_box .pkg_list ul").slimScroll({
            height: "266px",
            color: "#f78c0f"
        }), 0 != $(".scroll").length && $(".scroll").slimScroll({
            height: "100px",
            size: "6px",
            color: "#f78c0f",
            alwaysVisible: !0,
            distance: "16px",
            railVisible: !0,
            railColor: " rgb(209, 0, 29)",
            railOpacity: 1,
            railBorderRadius: "0px",
            wheelStep: 10,
            disableFadeOut: !1
        });
