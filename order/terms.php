<!DOCTYPE html>
<!doctype html>
<html lang="en">
<head>
  <title>Terms & Conditions </title>
<meta name="description"
          content="Get to know the terms and conditions that you need to follow in order to purchase copyrights from us.">
  
          <?php
  include __DIR__ . '/include/header.php'
  ?>


<section class="main-privacy">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 privacy">
                <div class="text-center">
                    <h4>Terms & Conditions</h4>
                </div>
                 <div class="term-text">
<p>Your use of this Website and/or our services constitutes your agreement to the following terms and conditions. If you do not agree with any of these terms and conditions, please do not use this Website or Our services. <br>If You are under legal age of consent for Your respective district, You are not allowed to access or use this Website or Our services. You acknowledge and agree that You must be of legal age to purchase any of Our products or services available on this Website or otherwise.<br>By submitting an order and/or payment, You are acknowledging that You have read and understand these terms and conditions. By submitting an order and/or payment, You are also agreeing to be legally bound by these terms and conditions, which form the entire agreement between You and <?= SITE_NAME ?>.</p>

<!-- <p>*Unlimited revisions require that the initial design and concept have to be the same, customer cannot ask for a redesign once an initial concept has been selected.</p> -->
<h3>DEFINITION OF TERMS</h3>
<p>“Website” means all online content on <?= SITE_NAME ?> website pages.<br>“Customer,” “You,” or “Yours” mean and refer to you and/or any other person submitting an order to <?= SITE_NAME ?> on your behalf.<br>“Company,” “We,” or “Our” mean and refer to <?= SITE_NAME ?>, a company registered under the laws.<br>“Product” means and refers to all services and products provided by <?= SITE_NAME ?> to Customer in accordance with his/her Order.<br>“Order” means an order via phone or e-mail made by Customer to purchase services or products provided by <?= SITE_NAME ?>. </p>
<h3>OUR SERVICES</h3>
<p>By submitting the Order and/or payment, You are purchasing the Product for Your use only. All Products are drafted by freelance writers who transferred all rights and ownership regarding the Products to the Company. All Products are non-refundable and come with no warranties, expressed or implied. It is Your obligation to read these Terms and Conditions before submitting any Order and/or payment to this Website.</p>
<h3>REFUND POLICY</h3>
<p>It is important that you read and fully understand the refund policy in order to have full knowledge of the privileges and limitations governed by the <?= SITE_NAME_TEXT_1 ?> Policy.<br>We offer refunds only in special cases and specified conditions, detailed as under:</p>
<h3>CHANGE OF MIND:</h3>
<p>•  The customer is entitled to 100% refund before our writers/editors start working on the project.</p>
<p>•  If (for any reason) you change your mind and decide against continuing your project with us after placing your order, you can ask for refund within the first hour of placing your order. A 15% processing fee will be charged in other cases.</p>
<h3>INCOMPETENT DELIVERY:</h3>
<p>•  Once the work is delivered, customers are only entitled to claim a refund once they have exhausted all the options detailed as under;</p>
<p>•  If it does not comply with project requirements (as requested/documented by the customer). We are committed to provide our customers with 100% satisfaction and offer unlimited revisions to ensure that the delivery is up to the mark. We assign, re-assign and re-write your work to ensure complete satisfaction.</p>
<p>•  Reserve FREE Pages for future, but of same value and you can avail them at any time.</p>
<p>If we’re still not able to deliver what you asked for, refund will be processed with a mutual agreement on a set percentage (but only in the cases where the delivery is completely off the mark)</p>
<h3>LATE DELIVERY:</h3>
<p>•  We believe in “On Time Delivery” but if, for any reason we fail to deliver the asked service on time, after at least three attempts to contact us, your refund will be processed once it is established through documentary evidence that the late delivery was the company’s fault.</p>
<h3>REFUND TIME FRAME:</h3>
<p>•  Refunds must be claimed within 120 days of delivery. Refunds claimed after the set time frame will not be entertained.</p>
<p>•  All the customers must note the deadline for claiming refund at the time of placing order.</p>
<h3>CASES WHERE REFUND WILL NOT BE ISSUED:</h3>
<p>•  In case of late delivery due to some minor technical errors, such as grammatical, typing, word count, missing references etc., refunds will be processed with mutual agreement and the company will only settle with partial refund or discounts reserved for future purchases.</p>
<p>•  The company will not be responsible for any delay from the client’s end.</p>
<p>•  No refund will be issued on the basis of low of writing.</p>
<h3>COMMUNICATION POLICY</h3>
<p>We at <?= SITE_NAME_TEXT_1 ?> value our customer’s privacy and we are highly committed to keeping client's personal information safe and secure. We collect only essential and inevitable information to process your order. <br>All information is collected through <?= SITE_NAME_TEXT_1 ?>.</p>
                </div>         
                
                         
            </div>
        </div>
    </div>
</section>

<?php
  include __DIR__ . '/include/footer.php'
  ?>