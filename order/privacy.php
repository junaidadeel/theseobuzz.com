<!DOCTYPE html>
<!doctype html>
<html lang="en">
<head>
  <title>Privacy Policy</title>
<meta name="description"
          content="We care about the privacy of the data you share with us, which is why we are dedicated to protecting your privacy.">
  
          <?php
  include __DIR__ . '/include/header.php'
  ?>

<section class="main-privacy">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 privacy">
                <div class="term-text">
                    <h3>About The Policy</h3>
<p>This notice discloses the privacy practices for <?= SITE_NAME ?>. This privacy notice applies solely to information collected by this website. It will notify you of the following: What personally identifiable information is collected from you through the website, how it is used, and with whom it may be shared.
</p>
<p>What choices are available to you regarding the use of your data.</p>
<p>The security procedures in place to protect the misuse of your information.<br>How you can correct any inaccuracies in the information. <br>Information collection, use, and sharing. <br>Confidentiality of services.<br>Originality of content.</p>
<p>We are the sole owners of the information collected on this site. We only have access to information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p>
<p>We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request.</p>
<p>Unless you ask us not to, we may contact you via email or phone to tell you about specials, new products or services, or changes to this privacy policy.</p>
<h3>YOUR ACCESS TO AND CONTROL OVER INFORMATION</h3>
<p>You may opt out of any future contacts from us at any time. You can do any of the following actions at any time by contacting us via the email address or phone number provided on our website:</p>
<p>See what data we have about you, if any.<br>Change/correct any data we have about you.<br>Have us delete any data we have about you.<br>Express any concern you have about our use of your data.</p>
<h3>SECURITY</h3>
<p>We protect your information offline. Only employees and contractors who need the information to perform a specific job (for example, billing or the services ordered) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>
<p>We at <?= SITE_NAME_TEXT_1 ?> value our customers’ privacy and we are highly committed to keeping client's personal information safe and secure. We collect only essential and inevitable information to process your order. We never share any of your information with any individual or company in any way until you agree to permit us for doing so.</p>
<p>All information is collected through <?= SITE_NAME_TEXT_1 ?> uses highly secured platforms to ensure there is no breach of data.</p>
<h3>PAYMENT</h3>
<p>In order to process your order, we may need to collect personal information about you, such as your name, email address, billing address, and phone number). Credit card information is only handled by our third-party credit card provider (PayPal.com or Square).</p>
<h3>IP ADDRESS AND COOKIES</h3>
<p>Non-personal information such as your IP address is automatically recorded when you enter our website. This information is used only to diagnose server problems and site administration. Cookies are used on this site to enhance user experience and for collecting usage statistics.</p>
<h3>INFORMATION ABOUT OUR PRODUCTS AND SERVICES</h3>
<p>We make every effort to be as accurate as possible in describing the services and products offered on <?= SITE_NAME ?>. We do not warrant that the product or service descriptions, or any other content on this site, is accurate, complete, reliable, current, or free of error. Please call or email us immediately if you feel that the product or service you received from us differs from the descriptions on this website.</p>
<p>If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at <?= SITE_PHONE_NUMBER_TEXT ?> or contact us via email at <?= SITE_INFO_EMAIL ?>.</p>
<h3>CONFIDENTIALITY AND OWNERSHIP</h3>
<p>While many writing services require recognition or sales incentives, our service is 100% confidential. All communication and content developed or reviewed for the purposes of working together will never be disclosed to any other party under any circumstances. We reserve no rights to the products and services we provide. Everything we edit or write is considered the sole property of the client. If you contract with us, you, and only you, will be recognized as the author and/or owner of your material.</p>
<h3>ORIGINALITY</h3>
<p>All original content we develop/ghostwrite for a client will be free of plagiarism and not copied from any other source. Any and all information submitted to a client that is inspired or researched from another source will be paraphrased and properly cited to ensure the material.</p>

                    
                </div> 
                
            </div>


        </div>
    </div>
</section>


<?php
  include __DIR__ . '/include/footer.php'
  ?>
  