<!doctype html>
<html lang="en">
   <head>
      <title>The Seo Buzz</title>
      <meta name="description" content="">

    <?php
    include __DIR__ . '/include/header.php'
    ?>

    <section class="main_banner" style="background-image: url(images/webp/inner_banner/contact-us.webp);  height: 421px;">
        <div class="container">
            <div class="row align-items-center justify-content-center justify-content-xl-between justify-content-lg-between">
                <div class="col-lg-6">
                    <div class="bnr_content" data-aos="fade-right" data-aos-duration="1500">
                        <h3><span class="text_1">Contact Us</span></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services sec_pt">
        <div class="container">
            <div class="row no-gutters responsive_slider_service">
                <div class="col-lg-4">
                    <div class="contact-us service_box hvr-buzz-out" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="100">
                        <div class="serv_icon">
                            <img src="images/webp/cont-serv-icon1.webp" alt="">
                        </div>
                        <h3>Call Us</h3>
                        <a href="tel:<?= SITE_PHONE_NUMBER ?>"><?= SITE_PHONE_NUMBER ?></a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="contact-us service_box hvr-buzz-out" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="400">
                        <div class="serv_icon">
                            <img src="images/webp/cont-serv-icon2.webp" alt="">
                        </div>
                        <h3>Address</h3>
                        <a href="javascript:;"><?= SITE_ADDRESS ?></a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="contact-us service_box hvr-buzz-out" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="700">
                        <div class="serv_icon">
                            <img src="images/webp/cont-serv-icon3.webp" alt="">
                        </div>
                        <h3>Email</h3>
                        <a href="mailto:<?= SITE_INFO_EMAIL ?>"><?= SITE_INFO_EMAIL ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="about_service">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="sec_title" data-aos="fade-right" data-aos-duration="1500">
                        <h3>We Are Here To Help You. If You Have Any Question,<br><span>Let Us Know!</span></h3>
                        <div class="form_area cont_form" data-form-type="contact_form">
                        <form class="contact-us-form leadForm" id="regForm" method="post" enctype="multipart/form-data"  action="javascript:void(0)" >
                            <!--hidden required values-->
                            <input type="hidden" id="formType" name="formType">
                            <input type="hidden" id="referer" name="referer">
                            <div class="row">
                                <div class="form-group col-md-6 ">
                                    <input type="text" class="form-control" name="name" placeholder="Full Name" data-validation="required">
                                </div>
                                <div class="form-group col-md-6 ">
                                    <input type="email" class="form-control" placeholder="Email" name="email" data-validation="required">
                                </div>
                                <div class="form-group col-md-6 ">
                                    <input type="tel" class="form-control phone" placeholder="Phone No" name="phone" maxlength="10" data-validation="required">
                                </div>
                                <div class="form-group col-md-6 ">
                                    <input type="text" class="form-control" name="customers_meta[website]" placeholder="Service">
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea placeholder="Message" name="customers_meta[message]"class="form-control " id="exampleFormControlTextarea1"></textarea>
                            </div>
                            <div class="form_btn_area">
                                <div id="formResult"></div>
                                <div class="clearfix"></div>
                                <button type="submit"  id="signupBtn" class="form_btn cont_form_btn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
                <div class="col-md-6">
                    <div class="about_serv_img">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3027.365912512294!2d-73.96914878432781!3d40.64386374971758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25b309f0e9417%3A0x20aaf03205f9c8f7!2s1226%20Beverley%20Rd%2C%20Brooklyn%2C%20NY%2011218%2C%20USA!5e0!3m2!1sen!2s!4v1607434738599!5m2!1sen!2s" width="600" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>


     <?php include __DIR__ . '/include/footer.php' ?>