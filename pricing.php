<!doctype html>
<html lang="en">
   <head>
      <title>The Seo Buzz</title>
      <meta name="description" content="">

    <?php
    include __DIR__ . '/include/header.php'
    ?>

    <section class="main_banner" style="background-image: url(images/webp/inner_banner/e-commerce.webp);  height: 790px;">
        <div class="container">
            <div class="row align-items-center justify-content-center justify-content-xl-between justify-content-lg-between">
                <div class="col-lg-6">
                    <div class="bnr_content" data-aos="fade-right" data-aos-duration="1500">
                       <!--  <h5>Manual</h5> -->
                        <h1>
                            <span class="text_1">Pricing Plans</span> <!-- <br>Service -->
                        </h1>
                        <p> At SEO Buzz, we believe in having services best suitable for everyone. Building upon this motto, our service plans ensure higher visibility for your website via effective search engine optimization. Sign up today!</p>
                        <a href="javascript:;" data-toggle="modal" data-target=".bd-example-modal-lg" data-toggle="modal" data-target=".bd-example-modal-lg" class="default_btn">Let's Get Started</a>
                        <img src="images/webp/rewards.webp" alt="">
                    </div>
                </div>
                <div class="col-lg-5 col-md-9">
                    <?php include __DIR__ . '/include/inner_form.php' ?>
                </div>
            </div>
        </div>
    </section>



    <section class="packages sec_pt sec_pb">
        <div class="container">
            <div class="sec_title" data-aos="fade-down" data-aos-duration="1500">
                <h3>Choose Your Pricing Plan</h3>
                <p>We offer our esteemed, global clients a diverse range of convenient digital marketing <br>solutions at competitively, affordable prices.</p>
            </div>
            <div class="row responsive_slider_pkg" data-aos="fade-up" data-aos-duration="1500">
                <?php
              $packages = getPackagesByCategory(CATEGORY_SEO_ID);
              $advancePackages = [];
              if(count($packages)){ ?>
            <?php foreach ($packages as $package) {
                 if ($package['is_advance']) {
                     $advancePackages[] = $package;
                 } else { ?>
               <div class="col-lg-4">
                  <?php echo generatePackageBox('package_box', $package); ?>
               </div>
                <?php } ?>
                <?php } ?>
        <?php } ?>
            </div>
            <div class="sec_btn">
                <a href="javascript:;" data-toggle="modal" data-target=".bd-example-modal-lg" class="default_btn">View All Packages</i></a>
            </div>
        </div>
        <div class="pkg_before">
            <img src="images/webp/pkg_before.webp" alt="">
        </div>
        <div class="pkg_after">
            <img src="images/webp/pkg_after.webp" alt="">
        </div>
    </section>

    <?php include __DIR__ . '/include/inner_cta.php' ?>

    <?php include __DIR__ . '/include/faq.php' ?>

    <?php include __DIR__ . '/include/testimonial.php' ?>

    <?php include __DIR__ . '/include/footer.php' ?>
