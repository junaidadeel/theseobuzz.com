<?php
   require_once __DIR__."/../crm_inc/core/config.php";
?>  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" href="/images/webp/fav.webp" type="image/png">
    <link rel="stylesheet" type="text/css" href="/css/plugin.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics old -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132895386-32"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-132895386-32');
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-64DKQZM7RT"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-64DKQZM7RT');
    </script>

    <script type="text/javascript">
        (function(c,l,a,r,i,t,y){
            c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
            t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
            y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
        })(window, document, "clarity", "script", "725ucz1ihb");
    </script>

    <script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "Organization",
    "name": "The Seo Buzz",
    "alternateName": "Seo Buzz",
    "url": "https://www.theseobuzz.com/",
    "logo": "https://www.theseobuzz.com/images/webp/logo.webp",
    "contactPoint": {
        "@type": "ContactPoint",
        "telephone": "(877) 403-1063",
        "contactType": "customer service",
        "contactOption": "TollFree",
        "areaServed": "US",
        "availableLanguage": "en"
    },
    "sameAs": [
        "https://www.facebook.com/The-Seo-Buzz-101844468505701/about/?ref=page_internal",
        "https://twitter.com/TheSeoBuzz",
        "https://www.youtube.com/channel/UCc0QcJoSA8pl_4mIkmUaJMQ",
        "https://www.pinterest.com/TheSeoBuzz/",
        "https://theseobuzz.tumblr.com/",
        "https://www.theseobuzz.com/"
    ]
    }
    </script>
    
    <script>
(function(w,d,s,r,n){w.TrustpilotObject=n;w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)};
a=d.createElement(s);a.async=1;a.src=r;a.type='text/java'+s;f=d.getElementsByTagName(s)[0];
f.parentNode.insertBefore(a,f)})(window,document,'script', 'https://invitejs.trustpilot.com/tp.min.js', 'tp');
tp('register', 'AxKCzSER4aQeGGTu');
</script>


<script>
  !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="bgyYT5yUmg2l7m9PyMICLS0UacZQReAj";;analytics.SNIPPET_VERSION="4.15.3";
  analytics.load("bgyYT5yUmg2l7m9PyMICLS0UacZQReAj");
  analytics.page();
  }}();
</script>

</head>
<body>
    <header class="main_header">
        <nav class="navbar navbar-expand-lg navbar-light" data-aos="fade-down" data-aos-duration="1500">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">
                    <img class="logo horizontal-logo" src="/images/webp/logo.webp" alt="Brand Logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about-us">About</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Services
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                               <li> <a class="dropdown-item" href="/content-marketing">Content Marketing</a></li>
                               <li class="sub-menu"><a class="dropdown-item dropdown-toggle" href="/e-commerce-seo">E-Commerce Seo</a>
                                   <ul class="dropdown-menu nest-sub-menu-ul">
                                        <li>  <a href="/magento-seo" class="dropdown-item">Magento Seo </a></li>
                                        <li> <a href="/shopify-seo" class="dropdown-item">Shopify Seo</a></li>
                                        <li> <a href="/bigcommerce-seo" class="dropdown-item">BigCommerce Seo</a></li>
                                        <li> <a href="/woocommerce-seo" class="dropdown-item">WooCommerce Seo</a></li>
                                    </ul>
                               </li>
                               <li class="sub-menu"><a class="dropdown-item dropdown-toggle" href="/on-page-seo">On-page SEO</a>
                                    <ul class="dropdown-menu nest-sub-menu-ul">
                                        <li>  <a href="/seo-audit" class="dropdown-item">SEO Audit</a></li>
                                        <li> <a href="/keyword-research" class="dropdown-item">Keyword Research</a></li>
                                    </ul>
                               </li>
                               <li class="sub-menu"><a class="dropdown-item dropdown-toggle" href="/off-page-seo">Off-page SEO</a>
                                    <ul class="dropdown-menu nest-sub-menu-ul">
                                        <li> <a class="dropdown-item"  href="/link-building">Link Building </a></li>
                                        <li><a class="dropdown-item" href="/local-seo">Local Seo</a></li>
                                        <li><a class="dropdown-item" href="/local-citation">Local Citations </a></li>
                                        <li><a class="dropdown-item" href="/guest-blogging">Guest Blogging  </a></li>
                                    </ul>
                               </li>                             
                               <li><a class="dropdown-item" href="/seo">SEO</a></li>
                               <li> <a class="dropdown-item" href="/social-media">Social Media</a></li>
                               <li> <a class="dropdown-item" href="/seo-reseller">SEO Reseller</a></li>
                               <li> <a class="dropdown-item" href="/website-content">Website Content</a></li>
                               <li> <a class="dropdown-item" href="/ppc">PPC</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/pricing">Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact-us">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/blog">Blog</a>
                        </li>
                    </ul>
                </div>
                <div class="header_btn">
                  <a href="javascript:;" data-toggle="modal" data-target=".bd-example-modal-lg" class="default_btn">Request A Quote <i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
        </nav>
    </header>