    <footer class="sec_pt sec_pb">
        <div class="container">
            <div class="help">
                <h3 data-aos="fade-right" data-aos-duration="1500">Have any questions? Let us know!  <br>We are here to help.</h3>
                <a href="javascript:;" data-toggle="modal" data-target=".bd-example-modal-lg" class="default_btn" data-aos="fade-left" data-aos-duration="1500">Still have a question?</i></a>
            </div>
            <div class="main_footer">
                <div class="row justify-content-between">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="ftr_cont">
                            <a href="/">
                                <img src="/images/webp/logo.webp" alt="">
                            </a>
                            <ul class="footer_links">
                                <li><a href="javascript:;"><?= SITE_ADDRESS ?></a></li>
                                <li><a href="tel:<?= SITE_PHONE_NUMBER ?>"><?= SITE_PHONE_NUMBER ?></a></li>
                                <li><a href="mailto:<?= SITE_INFO_EMAIL ?>">E-mail : <?= SITE_INFO_EMAIL ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6">
                        <div class="ftr_cont">
                            <h4>QUICK LINKS</h4>
                            <ul class="footer_links">
                                <li><a href="/about-us"><i class="fas fa-chevron-right"></i>About Us</a></li>
                                <!-- <li><a href="#"><i class="fas fa-chevron-right"></i>Portfolio</a></li> -->
                                <li><a href="/pricing"><i class="fas fa-chevron-right"></i>Pricing</a></li>
                                <li><a href="/contact-us"><i class="fas fa-chevron-right"></i>Contact </a></li>
                                <li><a href="/blog"><i class="fas fa-chevron-right"></i>Blog </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6">
                        <div class="ftr_cont">
                            <h4>SERVICES</h4>
                            <ul class="footer_links">
                                <li><a href="/seo"><i class="fas fa-chevron-right"></i>SEO</a></li>
                                <li><a href="/local-seo"><i class="fas fa-chevron-right"></i>Local SEO</a></li>
                                <li><a href="/e-commerce-seo"><i class="fas fa-chevron-right"></i>E-Commerce SEO</a>
                                    <ul class="footer_links subfoot_links">
                                        <li><a href="/magento-seo"><i class="fas fa-chevron-right"></i>Magento Seo</a></li>
                                        <li><a href="/shopify-seo"><i class="fas fa-chevron-right"></i>Shopify Seo</a></li>
                                        <li><a href="/bigcommerce-seo"><i class="fas fa-chevron-right"></i>BigCommerce Seo</a></li>
                                        <li><a href="/woocommerce-seo"><i class="fas fa-chevron-right"></i>WooCommerce Seo </a></li>
                                    </ul>
                                </li>
                                <li><a href="/link-building"><i class="fas fa-chevron-right"></i>Link Building</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-6">
                        <div class="ftr_cont">
                            <h4>SERVICES</h4>
                            <ul class="footer_links">
                                <li><a href="/social-media"><i class="fas fa-chevron-right"></i>Social Media</a></li>
                                <li><a href="/ppc"><i class="fas fa-chevron-right"></i>PPC</a></li>
                                <li><a href="/seo-audit"><i class="fas fa-chevron-right"></i>SEO Audit</a></li>
                                <li><a href="/website-content"><i class="fas fa-chevron-right"></i>Website Content</a></li>
                                <li><a href="/content-marketing"><i class="fas fa-chevron-right"></i>Content Marketing </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <section class="copyright">
        <div class="container">
            <div class="copyright_txt">
                <p>Copyright © <?php echo date('Y'); ?>. All right reserved. Design by <span><?= SITE_NAME_TEXT ?></span></p>
                <div class="footer-social-wrap">
                    <ul>
                        <li>
                            <a target="_blank" href="https://www.facebook.com/Theseobuzz/">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.youtube.com/channel/UCc0QcJoSA8pl_4mIkmUaJMQ">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.linkedin.com/in/the-seo-buzz-24bb56208/">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.pinterest.com/TheSeoBuzz/">
                                <i class="fab fa-pinterest-p"></i>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://twitter.com/TheSeoBuzz">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>



<div class="popup_form">
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="row no-gutters">
                        <div class="col-lg-6">
                            <div class="popup_left">
                                <div class="popup_form_box" data-form-type="popup_form">
                                    <form class="leadForm" id="regForm" method="post" enctype="multipart/form-data"  action="javascript:void(0)" >
                                        <!--hidden required values-->
                                        <input type="hidden" id="formType" name="formType">
                                        <input type="hidden" id="referer" name="referer">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Enter Your Name" data-validation="required">
                                            <i class="fas fa-user"></i>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="Enter Your Email" data-validation="required">
                                            <i class="fas fa-envelope"></i>
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" class="form-control phone" name="phone" maxlength="12" placeholder="Phone Number" data-validation="required">
                                            <i class="fas fa-phone"></i>
                                        </div>
                                        <div class="popup_btn">
                                            <h3>Seal The Limited Time Offer <br>Before It Expires</h3>
                                            <div id="formResult"></div>
                                            <div class="clearfix"></div>
                                            <button type="submit" id="signupBtn">Let's Get Started</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 d-none d-lg-block d-xl-block">
                            <div class="popup_right">
                                
                            </div>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
</div>

    <script src="/js/plugin.js"></script>
    <?php include_once __DIR__ . "/../crm_inc/assets.php"; ?>
    <?php include_once __DIR__ . '/../crm_inc/lead-setup.php'; ?>
    <?php include_once __DIR__ . '/../crm_inc/pricing-setup.php'; ?>
</body>
</html>