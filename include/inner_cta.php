<section class="inner_cta">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-md-7 col-sm-7">
                <div class="inner_cta_title">
                    <h3>EXCEPTIONAL <br><span>SEO SERVICES AT</span></h3>
                    <h2>75% <br><span>OFF</h2>
                </div>
            </div>
            <div class="col-md-3 col-sm-5">
                <div class="inner_cta_btn">
                    <p>GET IN TOUCH <br>WITH OUR EXPERTS</p>
                    <a href="javascript:;" class="chatt">Chat With Us</a>
                </div>
            </div>
        </div>
    </div>
    <div class="inner_cta_left">
        <img src="images/webp/inner_cta_left.webp" alt="" class="img-fluid">
    </div>
    <div class="inner_cta_right">
        <img src="images/webp/inner_cta_right.webp" alt="">
    </div>
</section>