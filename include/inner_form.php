<div class="banner_form">
                        <div class="form_title">
                            <h5>SIGN UP TO</h5>
                            <h4>AVAIL <span>75%</span></h4>
                            <h3>DISCOUNT</h3>
                        </div>
                        <div class="form_area" data-form-type="signup_form">
                        <form class="leadForm" id="regForm" method="post" enctype="multipart/form-data"  action="javascript:void(0)" >
                            <!--hidden required values-->
                            <input type="hidden" id="formType" name="formType">
                            <input type="hidden" id="referer" name="referer">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Your Name" data-validation="required">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" name="email" data-validation="required">
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control phone" placeholder="Phone" name="phone" maxlength="10" data-validation="required">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="customers_meta[website]" placeholder="Website">
                            </div>
                            <div class="form-group messege_field" id="messagefield">
                                <textarea placeholder="Message" name="customers_meta[message]"class="form-control " id="exampleFormControlTextarea1"></textarea>
                            </div>
                            <div class="form_btn_area">
                                <button href="#" id="messageButton" class="message_btn">Enter Your Message</button>
                                <div id="formResult"></div>
                                <div class="clearfix"></div>
                                <button type="submit"  id="signupBtn" class="form_btn">Submit</button>
                            </div>
                        </form>
                        </div>
                    </div>