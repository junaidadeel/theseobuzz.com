<section class="testimonial sec_pt sec_pb">
        <div class="container">
            <div class="sec_title" data-aos="fade-down" data-aos-duration="1500">
                <h3>Our Clients Loves Our Products</h3>
                <p>At The SEO Buzz, we prefer customer satisfaction over any financial asset that we own. This business has been built wisely with numerous clients' confidence and gratification, who enable us to continue in our footsteps. </p>
            </div>
            <div class="testi_slider owl-carousel owl-theme" data-aos="fade-up" data-aos-duration="1500">
                <div class="item">
                    <div class="testi_box">
                        <p>When I first joined The SEO Buzz as a client, I was blown away by the level of commitment and quality that is put in towards achieving the best end result for their customers!</p>
                        <div class="client_detail">
                            <img src="images/webp/client_img1.webp" alt="">
                            <div class="client_name">
                                <h4>Claire Kennedy</h4>
                                <p>Brand Manager</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="testi_box">
                        <p>My newly developed business website was facing severe issues in being optimized fully. Until I acquired the services of The SEO Buzz, I was disappointed. Now, I am so glad that I chose them as my business is reaching heights never attained previously.</p>
                        <div class="client_detail">
                            <img src="images/webp/client_img2.webp" alt="">
                            <div class="client_name">
                                <h4>Allen Smith</h4>
                                <p>CEO</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="item">
                    <div class="testi_box">
                        <p>My newly developed busines website was facing severe issues in being optimized fully. Until I accquired the services of SEO Pro Buzz, I was disappointed. Now, I am so glad that I chose them as my business is reaching heights never attained previously. </p>
                        <div class="client_detail">
                            <img src="images/webp/client_img2.webp" alt="">
                            <div class="client_name">
                                <h4>Claire Kennedy</h4>
                                <p>UI/UX Designer</p>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="item">
                    <div class="testi_box">
                        <p>I have been using several SEO services for my digital brands throughout my career as a marketer. Rest assured, I can easily say that this has been the easiest and the most effective platform to use as! Will definitely recommend them to anyone looking to boost their platform!</p>
                        <div class="client_detail">
                            <img src="images/webp/client_img3.webp" alt="">
                            <div class="client_name">
                                <h4>Jaime Beno</h4>
                                <p>Web Developer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>