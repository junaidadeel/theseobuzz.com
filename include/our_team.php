<section class="our_team sec_pt sec_pb">
        <div class="container">
            <div class="sec_title" data-aos="fade-down" data-aos-duration="1500">
                <h3>Our Proud Team</h3>
                <p>Holistically, we are a family of over 400 strategists, analysts, content optimizers and industry professionals with proficiency in building businesses and commerce across the digital domain</p>
            </div>
            <div class="row responsive_slider_projects">
                <div class="col-lg-3">
                    <div class="team_box" data-aos="fade-right" data-aos-duration="1500" data-aos-delay="100">
                        <div class="employee_img">
                            <img src="images/webp/team_img1.webp" alt="">
                        </div>
                        <h4>Tanya Figueroa</h4>
                        <p>UI/UX Designer</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="team_box" data-aos="fade-right" data-aos-duration="1500" data-aos-delay="300">
                        <div class="employee_img">
                            <img src="images/webp/team_img2.webp" alt="">
                        </div>
                        <h4>Claire Kennedy</h4>
                        <p>Web Developer</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="team_box" data-aos="fade-left" data-aos-duration="1500" data-aos-delay="300">
                        <div class="employee_img">
                            <img src="images/webp/team_img3.webp" alt="">
                        </div>
                        <h4>Kathryn Baker</h4>
                        <p>Marketing Manger</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="team_box" data-aos="fade-left" data-aos-duration="1500" data-aos-delay="300">
                        <div class="employee_img">
                            <img src="images/webp/team_img4.webp" alt="">
                        </div>
                        <h4>Connie Becker</h4>
                        <p>Apps Developer</p>
                    </div>
                </div>
            </div>
        </div>
    </section>