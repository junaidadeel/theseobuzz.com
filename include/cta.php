<section class="cta">
    <div class="container">
        <div class="cta_content">
            <div class="cta_title" data-aos="fade-right" data-aos-duration="1500">
                <h3>LET'S GET YOU RANKED</h3>
                <p>Talk To Our Seo Strategist And Get Started With Your Project Now</p>
            </div>
            <a href="javascript:;" class="default_btn" data-aos="fade-left" data-aos-duration="1500">View All Packages</i></a>
        </div>
    </div>
</section>