<section class="projects sec_pt sec_pb">
        <div class="container">
            <div class="row responsive_slider_projects">
                <div class="col" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="100">
                    <div class="project_box hvr-buzz-out">
                        <img src="images/webp/projects_icon1.webp" alt="">
                        <h5>Completed Projects</h5>
                        <h4>3942</h4>
                    </div>
                </div>
                <div class="col" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="300">
                    <div class="project_box hvr-buzz-out">
                        <img src="images/webp/projects_icon2.webp" alt="">
                        <h5>Ongoing Projects</h5>
                        <h4>5972</h4>
                    </div>
                </div>
                <div class="col" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="700">
                    <div class="project_box hvr-buzz-out">
                        <img src="images/webp/projects_icon3.webp" alt="">
                        <h5>Happy Clients</h5>
                        <h4>9384</h4>
                    </div>
                </div>
                <div class="col" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="1300">
                    <div class="project_box hvr-buzz-out">
                        <img src="images/webp/projects_icon4.webp" alt="">
                        <h5>Cup Of Coffee</h5>
                        <h4>9872</h4>
                    </div>
                </div>
                <div class="col" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="1600">
                    <div class="project_box hvr-buzz-out">
                        <img src="images/webp/projects_icon5.webp" alt="">
                        <h5>Seo Experts</h5>
                        <h4>492</h4>
                    </div>
                </div>
            </div>
            <div class="cta_content">
                <div class="cta_title" data-aos="fade-right" data-aos-duration="1500">
                    <h3>LET'S GET YOU RANKED</h3>
                    <p>Talk To Our Seo Strategist And Get Started With Your Project Now</p>
                </div>
                <a href="javascript:;" data-toggle="modal" data-target=".bd-example-modal-lg" class="default_btn" data-aos="fade-left" data-aos-duration="1500">REQUEST A QUOTE</i></a>
            </div>
        </div>
    </section>