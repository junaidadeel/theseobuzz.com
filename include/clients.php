<section class="clients sec_pt sec_pb">
        <div class="container">
            <div class="sec_title" data-aos="fade-down" data-aos-duration="1500">
                <h3>Our Proud Clientele</h3>
                <p>The SEO Buzz is renowned for attaining customer satisfaction by producing thorough results like no other! Rest assured, our experienced and skilled experts are working round-the-clock to ensure the growth of our clients.  </p>
            </div>
            <div class="client_img text-center" data-aos="fade-up" data-aos-duration="1500">
                <ul class="clients_slider">
                    <li><a href="https://remodeldoorandmore.com/" target="blank"><img src="images/webp/client-01.webp" alt=""></a></li>
                    <li><a href="https://texasketoeats.com/" target="blank"><img src="images/webp/client-02.webp" alt=""></a></li>
                    <li><a href="https://www.kathakali.net/" target="blank"><img src="images/webp/client-03.webp" alt=""></a></li>
                    <li><a href="https://www.happyflowerdaycare.com/" target="blank"><img src="images/webp/client-04.webp" alt=""></a></li>
                    <li><a href="https://upaintstudio.com/" target="blank"><img src="images/webp/client-05.webp" alt=""></a></li>
                    <li><a href="https://grabngotacos.com/" target="blank"><img src="images/webp/client-06.webp" alt=""></a></li>
                </ul>
            </div>
        </div>
    </section>