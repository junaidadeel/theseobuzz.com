<?php require_once __DIR__."/crm_inc/core/config.php"; ?>

<?php
   // $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
   // $uri_segments = explode('/', $uri_path);
   $slug = $_GET['id'];
   $articleData = getArticleById($slug);
   $article = $articleData['data']; 

?>

<!DOCTYPE html>
<html lang="en-US">
   <head>

   <?php if($slug){?>

      <title><?= $article['meta_title'] ?></title>
      <meta name="description" content="<?= $article['meta_description'] ?>">
      <meta name="keywords" content="<?= $article['tags'] ?>">
      <meta name="author" content="<?= $article['user']['name'] ?>">
      <link rel="canonical" href="https://www.theseobuzz.com/blog/<?= $slug ?>"/>


      <!-- header include -->
      <?php include __DIR__ . '/include/header.php';?>

      <!-- Start Blog Detail Page -->

      <!-- breadcrumb -->
      <!-- <section class="custom-breadcrumb-wrap">
         <div class="container">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb custom-breadcrumb-inner">
                  <li class="breadcrumb-item">
                     <a href="/">theseobuzz</a>
                  </li>
                  <li class="breadcrumb-item" aria-current="page">
                     <a href="/blog">Blog</a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page"><?= $article['title'] ?></li>
               </ol>
            </nav>
         </div>
      </section> -->
      
      <!-- inner  article -->
      <div class="article-wrapper">
         <div class="container">
            <article class="blog-details">
               <div class="post-info">
                  <div class="name-month">
                     <a href="javascript:;">
                        <i>
                           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                              <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                           </svg>
                        </i>
                        <i>
                           <?= $article['user']['name']?>
                        </i>
                     </a>
                     <a href="javascript:;">
                        <i>
                           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-calendar3" viewBox="0 0 16 16">
                              <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
                              <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                           </svg>
                        </i>
                        <i>
                           <?= date("jS F Y",strtotime($article['created_at'])); ?>
                        </i>
                     </a>
                  </div>
                  <h1><?= $article['title'] ?></h1>
                  <figure class="position-relative">
                     <img src="<?= CRM_URL."/files/articles/". $article['image'] ?>"   alt="">
                     <div class="image-overlay-content">
                        <h4 class="image-content">
                           <?= $article['title']?>
                        </h4>
                        <div class="image-content-box">
                           <!-- <a href="index.php" class="box-logo"> -->
                              <img src="/images/blog-logo.png" class="content-img" alt="">
                           <!-- </a> -->
                        </div>
                     </div>
                  </figure>
                  <div class="row no-gutters">
                     <div class="col-lg-12 px-1">
                        <p><?= $article['article'] ?></p>
                     </div>
                     <div class="col-lg-3">
                        <!-- form -->
                      </div>
                  </div>
               </div> 
            </article>
         </div>        
      </div>

      <!-- End Blog Detail Page -->    

      <?php include __DIR__ . '/include/footer.php'?>  

   
<?php }else{ ?>

   
   <?php 
      $articles = getCategoriesArticles(); 
   ?>

      <title>The Seo Buzz Digital Marketing Blog.</title>
      <meta name="description" content="theseobuzz is a professional SEO agency that can help you rule the search engines, turbocharge your marketing, and skyrocket the business with expert SEO services.">
      <meta name="Keywords" content="theseobuzz, SEO">
      <link rel="canonical" href="https://www.theseobuzz.com/blog"/>
      
      
      <!-- header include -->
      <?php include __DIR__ . '/include/header.php' ?>


      <!-- Start Single Blog Page -->

      <!-- banner  -->
    <section class="main_banner" style="background-image: url(images/webp/inner_banner/content-marketing.webp);  height: 790px;">
        <div class="container">
            <div class="row align-items-center justify-content-center justify-content-xl-between justify-content-lg-between">
                <div class="col-lg-6">
                    <div class="bnr_content" data-aos="fade-right" data-aos-duration="1500">
                        <h1>
                            <span class="text_1">Expert-Endorsed</span> <br>Trade Secrets
                        </h1>
                        <p>What’s the latest buzz around SEO town? SEO experts and industry professionals offer invaluable insights into the ever-evolving world of Search Engine Optimization.</p>
                        <a href="javascript:;" data-toggle="modal" data-target=".bd-example-modal-lg" class="default_btn">Let's Get Started</a>
                        <img src="images/webp/rewards.webp" alt="">
                    </div>
                </div>
                <div class="col-lg-5 col-md-9">
                    <?php
                    include __DIR__ . '/include/inner_form.php'
                    ?>
                </div>
            </div>
        </div>
    </section>

      <!-- breadcrumb -->
    <!-- <section class="">
         <div class="custom-breadcrumb-wrap">
            <div class="container">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb custom-breadcrumb-inner">
                     <li class="breadcrumb-item">
                        <a href="/">theseobuzz</a>
                     </li>                 				
                     <li class="breadcrumb-item active">Blogs</li>
                  </ol>
               </nav>
            </div>
         </div>
    </section> -->

      <section class="blog">
         <div class="container">
            <div class="blog-by-category section-padding">
               <div class="blog-grid text-center equalHeightWrapper">
                  <div class="row">            
                  <?php foreach($articles['articleData'] as $article){?>
                     <div class="item  col-lg-4">
                        <a href="blog/<?=$article['slug']?>" class="news-content-block content-block">
                           <div class="img-container">
                              <img src="<?= CRM_URL."/files/articles/".$article['image']?>" alt="Project image" class="img-fluid">
                              <div class="image-overlay-content">
                                 <h4 class="image-content">
                                    <?= $article['title']?>
                                 </h4>
                                 <div class="image-content-box">
                                    <!-- <a href="index.php" class="box-logo"> -->
                                       <img src="/images/blog-logo.png" class="content-img" alt="">
                                    <!-- </a> -->
                                 </div>
                              </div>
                           </div>
                           <h5 class="equalHeight">
                              <span class="content-block__sub-title"><?= date("jS F Y",strtotime($article['created_at'])); ?></span>
                              <?= $article['title']?>
                           </h5>
                        </a>
                     </div>
                     <?php }?>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- End Single Blog Page -->

    
   

   <!-- footer -->
   <?php include __DIR__ . '/include/footer.php'?>  

<?php } ?>


