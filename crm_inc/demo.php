<style>
    .customalert{
        position: fixed;
        padding: 15px 20px;
        min-width: 180px;
        z-index: 99999;
        left: -35px;
        top: 50%;
        transform: rotate(270deg) translateY(-50%);
        text-align: center;
        background: rgba(242, 222, 222, 0.5)
    }
</style>

<?php if(!SITE_IS_ACTIVE){?> 
    <div class="alert alert-danger customalert">
        <?= "Demo Website"?>
    </div>
<?php }?>