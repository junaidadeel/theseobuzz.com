<!-- Main Requests Helper -->
<script src="<?php echo SITE_URL ?>/js/core/ajaxHelper.js"></script>

<!-- General Functions -->
<script src="<?php echo SITE_URL ?>/js/core/generalHelper.js"></script>

<!-- Sweet Alert -->
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL ?>/js/alert/sweetalert.css">
<script src="<?php echo SITE_URL ?>/js/alert/sweetalert.min.js"></script>

<script src="<?php echo SITE_URL ?>/js/core/briefManagement.js"></script>	