<form class="signupForm">

    <!--hidden required values-->
    <input type="hidden" id="formType" name="formType">
    <input type="hidden" id="referer" name="referer">

    <div class="form-group">
        <i class="fas fa-user"></i>
        <input type="text" name="name" class="form-control" placeholder="Enter Your Name">
    </div>
    <div class="form-group">
        <i class="fas fa-envelope"></i>
        <input type="email" name="email" class="form-control" placeholder="Email Address">
    </div>
    <div class="form-group">
        <i class="fas fa-phone"></i>
        <input type="tel" name="phone" placeholder="Phone Number" class="form-control required phone" maxlength="10" onkeyup="javascript: this.value = this.value.replace(/[^0-9]/g,'');">
    </div>
    <div class="form-group">
        <i class="fas fa-cog"></i>
        <select class="form-control" name="category_id">
            <option value="">Select Service</option>
            <?php foreach (getCategories() as $category){ ?>
                <option value="<?= $category->id ?>"><?= $category->name ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <div id="formResult"></div>
        <button type="submit" id="signupBtn" class="btn btn_red" value="1" name="signupForm">Reserve this discount</button>
    </div>
</form>