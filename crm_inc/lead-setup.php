<!-- Forms validation -->
<script src="<?php echo SITE_URL ?>/js/form_validator.min.js"></script>

<!-- Main Requests Helper -->
<script src="<?php echo SITE_URL ?>/js/core/ajaxHelper.js"></script>

<!-- General Functions -->
<script src="<?php echo SITE_URL ?>/js/core/generalHelper.js"></script>

<!-- Sweet Alert -->
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL ?>/js/alert/sweetalert.css">
<script src="<?php echo SITE_URL ?>/js/alert/sweetalert.min.js"></script>

<!-- Captcha Script -->
<!-- <script src="https://www.google.com/recaptcha/api.js?onload=onLoadCaptcha&render=explicit"></script> -->

<!-- Initialzing Script -->
<script>
	
	// validation Initializing
    $.validate({});
    
    // Captcha Key Initializing
    // var captchaKey = "<?php echo CAPTCHA_KEY ?>"

</script>

<!-- Lead Management Of Form -->
<script src="<?php echo SITE_URL ?>/js/core/leadManagement.js"></script>
