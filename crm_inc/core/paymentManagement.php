<?php

require_once __DIR__ . "/config.php";

$validationResponse = validateValues($_POST);

if ($validationResponse['status']) { 
    $data = $_POST;
    if(isset($data['invoiceId']) && $data['invoiceId'] == ""){
        unset( $data['invoiceId']);
    }

    $data['campaignId'] = CRM_CAMPAIGN_ID;
    $data['merchantId'] = CRM_MERCHANT_ID;
    if(getSessionValue('promotion_id', 'package_detail') > 0){
        $data['promotionId'] = getSessionValue('promotion_id', 'package_detail');
        $data['amount'] = getSessionValue('price', 'package_detail');
    }
    $data['currency'] = CRM_CURRENCY;
    $data['packageId'] = getSessionValue('id','package_detail');
    $data['customerId'] = getSessionValue('id','user_detail');
    $data['ordersMeta'] = getSessionValue('orders_meta');
    $result = Functions::createOrder($data);

    $tempResult = json_decode($result, true);
    
    if ($tempResult['success']) {
        $_SESSION['invoiceId'] = $tempResult['invoiceId'];
        echo json_encode(['status' => true, 'data' => $result]);
    } else {
        echo json_encode(['status' => false, 'data' => $result, 'messages' => ["Somthing went wrong on our end please contact support or chat"]]);
    }

}
else {
    echo json_encode(['status' => false, 'messages' => $validationResponse['errors']]);
}


function validateValues($values) {

    $errors = [];
    if ($values['customer']['firstName'] == "") {
        $errors[] = "First Name field is required";
    }
    if ($values['customer']['lastName'] == "") {
        $errors[] = "Last Name field is required";
    }
    if ($values['customer']['country'] == "") {
        $errors[] = "Country field is required";
    }
    if ($values['customer']['zip'] == "") {
        $errors[] = "Zip field is required";
    }
    if ($values['customer']['city'] == "") {
        $errors[] = "City field is required";
    }
    if ($values['customer']['state'] == "") {
        $errors[] = "State field is required";
    }
    if ($values['customer']['address'] == "") {
        $errors[] = "Address field is required";
    }
    if ($values['customer']['phone'] == "") {
        $errors[] = "Phone field is required";
    }
    if ($values['customer']['email'] == "") {
        $errors[] = "Email field is required";
    }
    if ($values['card']['name'] == "") {
        $errors[] = "Card Name field is required";
    }
    if ($values['card']['number'] == "") {
        $errors[] = "Card Number field is required";
    }
    if ($values['card']['expiryMonth'] == "") {
        $errors[] = "Expiry Month field is required";
    }
    if ($values['card']['expiryYear'] == "") {
        $errors[] = "Expiry Year field is required";
    }
    if ($values['card']['cvv'] == "") {
        $errors[] = "CVV field is required";
    }
    if (!empty($errors)) {
        return ["status" => false, "errors" => $errors];
    }
    return ["status" => true, "errors" => $errors];
}


// function getStates($countryId){
//     $states=[];
//     foreach($statesData as $value){
//         $states[$value['id']] = $value['name'];
//     }
//     return $states;
// }
?>