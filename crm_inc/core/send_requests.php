<?php

require_once __DIR__ . "/config.php";

if (isset($_POST['signupForm'])) {

    $data = $_POST;
    $data['ip'] = Functions::getClientIp();


    if(isset($data['formType']) && $data['formType'] == "ordernow_form"){
        $_SESSION['user_detail'] = $data;
    }

    $result =  Functions::addLead($data);

    if (isset($data['formType']) && $data['formType'] == "ordernow_form") {

        $tempResult = json_decode($result, true);
        if (isset($tempResult['success']) && $tempResult['success']) {
            $_SESSION['user_detail']['id'] = $tempResult['data']['leadId'];
        }

    }

    echo $result;

}

if (isset($_POST['orderSession'])) {

    $data = $_POST;

    $_SESSION['package_detail'] = $data;

    echo 1;

}

if (isset($_POST['briefForm'])) {

    $data = $_POST;

    if(isset($data['orders_meta'])){
        $_SESSION['orders_meta'] = $data['orders_meta'];
        Functions::sendEmail(serialize($data),"ORDER Brief Alert: ".date('D-M-Y',time()));
    }


    echo 1;

}

if (isset($_POST['orderForm'])) {

    $data = $_POST;

    if(isset($data['invoiceId']) && $data['invoiceId'] == ""){
        unset( $data['invoiceId']);
    }

    $data['campaignId'] = CRM_CAMPAIGN_ID;
    $data['merchantId'] = CRM_MERCHANT_ID;

    if(getSessionValue('promotion_id', 'package_detail') > 0){
        $data['promotionId'] = getSessionValue('promotion_id', 'package_detail');
        $data['amount'] = getSessionValue('price', 'package_detail');
    }


    $data['currency'] = CRM_CURRENCY;
    $data['customer']['country'] = 'GBR';
    $data['packageId'] = getSessionValue('id','package_detail');
    $data['customerId'] = getSessionValue('id','user_detail');

    $data['ordersMeta'] = getSessionValue('orders_meta');


    $result = Functions::createOrder($data);

    $tempResult = json_decode($result, true);
    if (isset($tempResult['success'])) {
        $_SESSION['invoiceId'] = $tempResult['invoiceId'];
    }

    echo $result;

}