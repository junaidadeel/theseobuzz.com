<?php

function getCategories()
{
    $categories = json_decode(Functions::getCategories());
    $data = [];
    if ($categories->success) {
        $data = $categories->data;
    }
    return $data;
}

function getFeaturedPackages()
{
    $packages = json_decode(Functions::getPackages(["isFeatured" => 1]), true);

    $data = [];

    if (isset($packages['success']) && $packages['success']) {
        $data = $packages['data'];
    }

    return $data;
}

function getPackagesByCategory($category_id, $isFeatured = false)
{
    
    $postData = ['categoryId' => $category_id];
    
    if ($isFeatured) {
        $postData['isFeatured'] = 1;
    }
    $packages = json_decode(Functions::getPackages($postData), true);
    
    $data = [];

    if (isset($packages['success']) && $packages['success']) {
        $data = $packages['data'];
    }

    return $data;
}

function getCategoriesPackages()
{
    $categoriesPackages = json_decode(Functions::getCategoriesPackages(), true);

    $data = [];

    if (isset($categoriesPackages['success']) && $categoriesPackages['success']) {
        $data = $categoriesPackages['data'];
    }

    return $data;
}

function getPromotionPackages($promotion_id, $forTabs = false)
{

    $promotionPackages = json_decode(Functions::getPromotionPackages(['promotionId' => $promotion_id]), true);

    $data = [];

    if (isset($promotionPackages['success']) && $promotionPackages['success']) {

        if ($forTabs) {
            $data = promotionDataWithTabs($promotionPackages['data']);
        } else {
            $data = $promotionPackages['data'];
        }

    }

    return $data;

}

function promotionDataWithTabs($data)
{

    $return = [];

    foreach ($data as $k => $val) {

        if (!isset($return[$val['category']['id']])) {
            $return[$val['category']['id']] = $val['category'];
        }
        $return[$val['category']['id']]["package"][] = $val;

    }

    $return = array_values($return);

    return $return;
}

function generatePackageBox($file, $data)
{
    $output = NULL;

    $filePath = __DIR__ . '/../package_boxes/' . $file . '.php';

    if (file_exists($filePath)) {
        // Extract the variables to a local namespace
        extract($data);

        // Start output buffering
        ob_start();

        // Include the template file
        include $filePath;

        // End buffering and return its contents
        $output = ob_get_clean();
    }

    return $output;
}

function getSessionValue($key, $parentKey = false)
{
    if (isset($_SESSION[$key])) {        
        return $_SESSION[$key];
    } else if ($parentKey && isset($_SESSION[$parentKey]) && $_SESSION[$parentKey][$key]) {
        return $_SESSION[$parentKey][$key];
    }

    return "";

}

function getDescriptor() {
    $merchantDetail = Functions::getMerchantDetail(CRM_MERCHANT_ID);
    if (count($merchantDetail)) {
        return $merchantDetail->descriptor;
    }
}

function getWebsiteMeta()
{
    $webData = json_decode(Functions::getWebsiteMeta(),true);
    $data = [];
    if ($webData['success']) {
        $data = $webData['data'];
    }
    return $data;
    
}

    function getDiscountPercentage($price, $crossPrice) {
        return round((($crossPrice - $price) * 100) / $crossPrice) . "% OFF";
    }

    function getCountries() {

        $countriesData = json_decode(Functions::getCountries(), true);
        return $countriesData['data'];
    }


    function getCategoriesArticles()
{
    $categories = json_decode(Functions::getCategoriesArticles(), true);    
    
    return $categories;
}

function getArticleByCategory($categoryId)
{    

    $articlesByCategories = json_decode(Functions::getArticleByCategory(['categoryId' => $categoryId]), true);    

    return $articlesByCategories;
}

function getArticleById($slug)
{
     
    $articlesBySlug = json_decode(Functions::getArticleById(["slug"=>$slug]), true);    
  
    return $articlesBySlug;
}











?>
