<?php

require_once __DIR__ . "/config.php";

$validationResponse = validateValues($_POST);
if ($validationResponse['status']) {
    
    
    $data = $_POST;
    $data['ip'] = Functions::getClientIp();

    $result = Functions::addLead($data);

    $tempResult = json_decode($result, true);

    if (isset($data['formType']) && $data['formType'] == "ordernow_form") {
        $_SESSION['user_detail'] = $data;
        if (isset($tempResult['success']) && $tempResult['success']) {
            $_SESSION['user_detail']['id'] = $tempResult['data']['leadId'];
        }
    }
    if (isset($tempResult['success']) && $tempResult['success']) {

        echo json_encode(['status' => true, 'data' => $result]);
    } else {
        echo json_encode(['status' => false, 'data' => $result, 'messages' => ["Somthing went wrong on our end please contact support or chat"]]);
    }
} else {
    echo json_encode(['status' => false, 'messages' => $validationResponse['errors']]);
}



function validateValues($values) {

    $errors = [];
    if ($values['name'] == "") {
        $errors[] = "Name field is required";
    }
    if ($values['email'] == "") {
        $errors[] = "Email field is required";
    }
    if ($values['phone'] == "") {
        $errors[] = "Phone field is required";
    }
    // if ($values['captcha'] == "") {
    //     $errors[] = "Captcha field is required";
    // }
    if (!empty($errors)) {
        return ["status" => false, "errors" => $errors];
    }
    return ["status" => true, "errors" => $errors];
}
