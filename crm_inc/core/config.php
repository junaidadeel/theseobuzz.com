<?php

session_start();

define('ENV_DEV', strpos($_SERVER['HTTP_HOST'], 'local') > -1 ? TRUE : FALSE);

if (ENV_DEV) {
    $PATH_BASE = $_SERVER['DOCUMENT_ROOT'] . '/../';
    $PATH_ENGINE = $PATH_BASE . "ledgecrmengine";
} else {
    $PATH_ENGINE = '/var/www/vhost/ledgecrmengine';
}

require_once $PATH_ENGINE . '/main.config.php';
require_once __DIR__ . '/functions.php';

// if ($debugMode || ENV_DEV) {
//     define("CRM_MERCHANT_ID", 1);
// } else {
//     define("CRM_MERCHANT_ID", 39);
// }

/**website api config**/
define("CRM_API_TOKEN", "frim5hMheC6wgeHakJ7bcDGkobuJWZQq0mUanLHhb14PoBhKRF0W88GZLCy6");
define("CRM_CAMPAIGN_ID", 2);
define("WEBSITE_ID", 179);

$webData =  getWebsiteMeta();

define("CRM_CURRENCY", $webData['website_currency']);
define('COUNTIRES', $webData['countries']);
define("CRM_MERCHANT_ID", $webData['merchantId']);

/**page category id**/
define("CATEGORY_SEO_ID",859);
define("CATEGORY_LINK_BUILDING_ID",860);
define("CATEGORY_SOCIAL_MEDIA_ID",861);
define("CATEGORY_WEB_CONTENT_ID",862);


/**page promotion id**/
// define("WEBSITE_DESIGN_PROMOTION_ID", 301);


if (ENV_DEV) {
    define('SITE_URL', 'http://local.theseobuzz.com');
    define('CRM_URL', 'https://ledgecrm.com');  
} else {
    define('SITE_URL', $webData['website_url']);
    define('CRM_URL', 'https://ledgecrm.com');  
}

/**website content vars i.e: Phone, email etc **/
// define('SITE_URL', $webData['website_url']);
define('SITE_NAME', $webData['website_name']);
define('SITE_NAME_TEXT', 'The Seo Buzz');
define('SITE_NAME_TEXT_1', 'The Seo Buzz');
define('SITE_PHONE_NUMBER_TEXT', $webData['number']);
define('SITE_PHONE_NUMBER', $webData['number']);
define('SITE_INFO_EMAIL', $webData['email']);
define('SITE_NO_REPLY_EMAIL', $webData['email']);
define('SITE_DISCUSS_EMAIL', $webData['email']);
define('SITE_CONTACT_EMAIL', $webData['email']);
define('SITE_CUSTOMER_SERVICE_EMAIL', $webData['email']);
define('SITE_SOLUTIONS_EMAIL', $webData['email']);
define('SITE_SUPPORT_EMAIL', $webData['email']);
define('SITE_ADDRESS', $webData['address']);
define('SITE_CURRENCY_SYMBOLS', '$');

