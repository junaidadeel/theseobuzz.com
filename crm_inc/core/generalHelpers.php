<?php

require_once __DIR__ . "/config.php";

if(isset($_POST['state'])){
    
    $states=getStates($_POST['countryId'],getCountries());
    if(count($states)>0){
        echo json_encode(['status'=>true, 'data'=> $states]);
    }
    else{
        echo json_encode(['status'=>false, 'data'=> null]);
    }   
}

function getStates($countryId,$countries){
    
    $states=[];
    foreach($countries as $values){
        if($values['code']==$countryId){
            foreach($values['states'] as $state){
                $states[$state['code']]=$state['name'];   
            } 
        }
    }
    return $states;
}