<div class="package_box" data-package-box>
    <div class="pkg_top">
        <div class="pkg_main">
            <div class="pkg_title">
               <div class="productSku" style="display: none;"><?= $sku ?></div>
                <h4><?= $title ?></h4>
            </div>
            <h3><?= $technical_description ?></h3>
        </div>
    </div>
    <div class="pkg_bottom">
        <div class="pkg_list" data-package-scroll>
            <?=$html_description?>
        </div>
        <div class="pkg_bottom_price">
            <div class="month_prie_left">
                <h4><span class="cutting_price"><?= $currency['symbol'];?><?= $cross_price ?></span><strong><?= $currency['symbol'];?></strong><?= ($price); ?><span>First Month Payment</span></h4>
            </div>
            <div class="month_prie_right">
                <p><?= $short_description ?></p>
            </div>
        </div>
        <a href="javascript:;" class="pkg_btn order-package"
         data-sku="<?= $sku ?>"
          data-promotion-id="<?= isset($promotion_id) ? $promotion_id : 0 ?>"
          data-price="<?= $price ?>"
          data-price-text="<?= $currency['symbol'];?><?= Functions::twoDecimalPlaces($price); ?>"
          data-title="<?=$title?>"
          data-package-id="<?= $id ?>">Get Started <i class="fas fa-arrow-right"></i></a>
    </div>
</div>